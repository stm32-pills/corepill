CorePill
========

Mithat Konar

CorePill is an easy to assemble, "Blue Pill"-compatible STM32F103C8T6 development board with only the most essential features: USB functionality and support for 5V/RAW voltage input have been removed.

![CorePill top](./images/CorePill-top.png)  ![CorePill top](./images/CorePill-bottom.png)

The board uses 0805 surface mount parts and a few through hole parts that can be easily obtained as generic components. All critical SMD assembly is on the top side of the board, making it possible to assemble the board at minimal cost. (There is one bypass capacitor on the bottom of the board, but it may not be needed.)

CorePill is intended as an alternative if your project doesn't need USB and on-board voltage regulation or if good Blue Pills become unavailable.

This work is derived from [STM32surface](https://github.com/profdc9/STM32surface) copyrighted by Daniel Marks (KW4TI) and licensed under the Creative Commons License CC-BY-SA 4.0.
